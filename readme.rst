Store And Forward Firmware Sample
=================================

In order to submit your measurements to the Store and Forward **SF** platform from an arduino
based board you will need to compile this project then burn the firmware in the board.

The sensor this guide will use are:
* DHT22
* SHT30
* DS18B20
* VALTAGE SENSOR
  
Platform IO
===========

PlatformIO allows developer to compile the same code with different development platforms using 
the Only One Command  platformio run. This happens due to Project Configuration File (platformio.ini) 
where you can setup different environments with specific options (platform type, firmware uploading
settings, pre-built framework, build flags and many more)
More info in <https://docs.platformio.org/en/latest/>

How to install Platform IO
==========================

1. Download and install VSCode <https://code.visualstudio.com/>
2. The install the Plugin PlatformIO <https://platformio.org/platformio-ide>
   
You will see a new icon in your VSCode tool bar, something similar to:

Clone the repository
====================

Run the following command in the console, this will create a new folder **sf-firmware**.
Clone the repo:

.. code-block::
$git clone https://gitlab.com/german.martinez/store-and-forward-documentation.git sf-firmware


Building Project
================

After clone the project you can proceed to build it:

1. click on the platformio icon
2. look for **Build** inside the General tab then click.

You will see the console running the building script.

Burn the Firmware to board
==========================

Make sure you have your board connected first. After the firmare is successfuly build you have to do click on 
the **Upload** option inside the platformIo section in the VScode, this again will run a console script and start 
uploading the firmware to the board. That is all, your board should be ready to start sending measurements 
to the SF platform.
